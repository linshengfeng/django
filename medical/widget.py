from django import forms

__all__ = (
    'DisplayNoneWidget'
)


class VaccinationModelWidget:
    pass


class DisplayNoneWidget(forms.widgets.Input):
    pass
